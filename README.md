# Anti-Adblock Blocker

:warning: **This add-on is not able to block all anti-adblock scripts, only
common solutions are supported for now** :warning:

See solutions section below.

## Install

Firefox: https://addons.mozilla.org/en-US/firefox/addon/anti-adblock-blocker/

Chrome: Coming soon...

## Solutions

| Common Name<sup>1<sup>      | Test Link                                              | Status           |
|-----------------------------|--------------------------------------------------------|------------------|
| AdBlocker Detector          | `http://www.lcomlearning.com/`                         | Supported        |
| AdBlock Notify              | `http://www.techstext.com/`                            | Supported        |
| Admiral Legacy              | `https://desktopsolution.org/`                         | Supported        |
| Admiral                     |                                                        | In Progress      |
| Link Shortener `app_vars`   | `http://shortlink.win/FQ2ZMj`                          | Supported        |
| BlockAdBlock Legacy         |                                                        | Supported (beta) |
| BlockAdBlock 4              | `https://blockadblock.com/`                            | Supported (beta) |
| Cloudflare Apps<sup>2</sup> | `https://www.cloudflare.com/apps/ngqhM7rZolNP/install` | Supported        |

<sup>1</sup> May not be the official name

<sup>2</sup> Only includes AdBlock Minus for now, can be easily expanded

## FAQ

### Does this work with all adblockers?

It should work with any reasonable adblockers, although I can't guarantee for
all. It's tested to be working with Adblock Plus and AdBlock.

### How does this compare to Anti-Adblock Killer or Nano Defender?

It's been years since Anti-Adblock Killer was last updated, so I'm pretty
confident that this will be better in a couple months.

As for Nano Defender, it only supports two adblockers. If you happen to use
those two, then chances are Nano Defender would be better, otherwise, it looks
like you can't use Nano Defender.

### This is breaking / slowing down a website, how do I whitelist?

I have plans to implement a whitelist, not sure how hard it will be though.

For now, you can open an issue report and I can add it to the hard-coded
whitelist.

## Permissions

| Technical                   | Description                              | Reason                                                     |
|-----------------------------|------------------------------------------|------------------------------------------------------------|
| `http://*/*`, `https://*/*` | Read and write all data for all websites | This is required to inject solutions to websites you visit |

## Contribute

Love this add-on?

- [Rate 5 stars! :star: :star: :star: :star: :star:](https://addons.mozilla.org/en-US/firefox/addon/anti-adblock-blocker/)
- Tell your friends
- Post on social media

Something's not right?

- [Open a new issue](https://gitlab.com/aforementioned.dude/anti-adblock-blocker/issues)

## License

I'll keep this close source for now. I'm worried that people will patch my
solutions too quickly otherwise.

I'm shipping the original TypeScript source with the add-on package, so you
can easily inspect it. If my solutions start to get patched, I may stop
shipping the original code, but don't worry, add-on reviewers will still have
access to the original code.
